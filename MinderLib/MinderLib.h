//
//  MinderLib.h
//  MinderLib
//
//  Created by Eric Yuen on 8/8/16.
//  Copyright © 2016 Eric Yuen. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <IOBluetooth/objc/IOBluetoothSDPUUID.h>
#import <IOBluetooth/objc/IOBluetoothDevice.h>
#import <IOBluetooth/objc/IOBluetoothRFCOMMChannel.h>
#import <IOBluetoothUI/objc/IOBluetoothDeviceSelectorController.h>

@protocol MinderLibDelegate
- (void)updateStatus:(int)status; //0 = disconnected, 1 = connecting, 2 = connected
- (void)updateCharge:(float)charge;
- (void)updatePosture:(float)x withY:(float)y withT:(double)t;
- (void)updateAccelWithX:(int)x withY:(int)y withZ:(int)z withT:(double)t;
- (void)updateGyroWithRoll:(int)roll withPitch:(int)pitch withYaw:(int)yaw withT:(double)t;
@end

@interface MinderLib : NSObject
- (id) initWithDelegate:(id <MinderLibDelegate>)delegate;

@property (readonly) id <MinderLibDelegate> dataDelegate;
@property (readonly) int status;
@property (readonly) float charge;

- (BOOL)connect:(IOBluetoothDevice *)device;
- (BOOL)connect:(IOBluetoothDevice *)device withDelegate:(id <MinderLibDelegate>)delegate;
- (void)disconnect;

- (BOOL)enableAccel;
- (BOOL)disableAccel;
- (BOOL)enableGyro;
- (BOOL)disableGyro;
- (BOOL)blinkLED;
- (BOOL)vibrateDevice;
- (BOOL)enableAngle;
- (BOOL)disableAngle;

@end
