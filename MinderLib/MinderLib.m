//
//  MinderLib.m
//  MinderLib
//
//  Created by Eric Yuen on 8/8/16.
//  Copyright © 2016 Eric Yuen. All rights reserved.
//

#import "MinderLib.h"

@implementation MinderLib {
    // Bluetooth variables:
    IOBluetoothDevice *mBluetoothDevice;
    IOBluetoothRFCOMMChannel *mRFCOMMChannel;
    
    int formerByte;
    BOOL packetFlag;
    int x, xFormer;
    int y, yFormer;
    int z, zFormer;
    int packetByteCounter;
    float dy, dz;
    float dyyFormer, dzzFormer;
    float displayX, displayY;
    int packetType;
}

- (id)init {
    self = [super init];
    if (self) {
        formerByte = 0;
        packetFlag = false;
        packetByteCounter = 0;
        xFormer = 0;
        yFormer = 0;
        zFormer = 0;
        dyyFormer = 0;
        dzzFormer = 0;
        dy = 0;
        dz = 0;
        packetType = 7;
        
    }
    return self;
    
}

- (id)initWithDelegate:(id<MinderLibDelegate>)delegate {
    _dataDelegate = delegate;
    [self.dataDelegate updateStatus:0];
    
    return [self init];
}


- (BOOL)connect:(IOBluetoothDevice *)device {
    return [self openSerialPortProfile:device];
}
- (BOOL)connect:(IOBluetoothDevice *)device withDelegate:(id<MinderLibDelegate>)delegate {
    _dataDelegate = delegate;
    [self.dataDelegate updateStatus:0];
    return [self openSerialPortProfile:device];
}

- (void)disconnect {
    [self closeDeviceConnectionOnDevice:mBluetoothDevice];
}

- (BOOL)enableAccel {
    NSLog(@"Enable Accel Received\n");
    [self writeCommandToDevice:@"w"];
    sleep(1);
    return [self writeCommandToDevice:@"h"];
}
- (BOOL)disableAccel {
    NSLog(@"Disable Accel Received\n");
    [self writeCommandToDevice:@"w"];
    sleep(1);
    return [self writeCommandToDevice:@"H"];
}
- (BOOL)enableGyro {
    NSLog(@"Enable Gyro Received\n");
    [self writeCommandToDevice:@"w"];
    sleep(1);
    return [self writeCommandToDevice:@"g"];
}
- (BOOL)disableGyro {
    NSLog(@"Disable Gyro Received\n");
    [self writeCommandToDevice:@"w"];
    sleep(1);
    return [self writeCommandToDevice:@"G"];
}

- (BOOL)blinkLED {
    NSLog(@"blink LED Received\n");
    [self writeCommandToDevice:@"w"];
    sleep(1);
    return [self writeCommandToDevice:@"l"];
}
- (BOOL)vibrateDevice {
    NSLog(@"vibrate Received\n");
    [self writeCommandToDevice:@"w"];
    sleep(1);
    return [self writeCommandToDevice:@"V"];
}
- (BOOL)enableAngle {
    NSLog(@"Enable Angle Received\n");
    [self writeCommandToDevice:@"w"];
    sleep(1);
    return [self writeCommandToDevice:@"h"];
}
- (BOOL)disableAngle {
    NSLog(@"Disable Angle Received\n");
    [self writeCommandToDevice:@"w"];
    sleep(1);
    return [self writeCommandToDevice:@"H"];
}


- (BOOL)openSerialPortProfile:(IOBluetoothDevice *)device
{
    IOBluetoothSDPUUID *sppServiceUUID;
    
    // Create an IOBluetoothSDPUUID object for the chat service UUID
    sppServiceUUID = [IOBluetoothSDPUUID uuid16:kBluetoothSDPUUID16ServiceClassSerialPort];
    
    // Finds the service record that describes the service (UUID) we are looking for:
    IOBluetoothSDPServiceRecord	*sppServiceRecord = [device getServiceRecordForUUID:sppServiceUUID];
    
    if ( sppServiceRecord == nil )
    {
        NSLog( @"Error - no spp service in selected device.  ***This should never happen since the selector forces the user to select only devices with spp.***\n" );
        return FALSE;
    }
    
    // To connect we need a device to connect and an RFCOMM channel ID to open on the device:
    UInt8	rfcommChannelID;
    if ( [sppServiceRecord getRFCOMMChannelID:&rfcommChannelID] != kIOReturnSuccess )
    {
        NSLog( @"Error - no spp service in selected device.  ***This should never happen an spp service must have an rfcomm channel id.***\n" );
        return FALSE;
    }
    
    // Open asyncronously the rfcomm channel when all the open sequence is completed my implementation of "rfcommChannelOpenComplete:" will be called.
    IOBluetoothRFCOMMChannel *channel;
    
    if ( ( [device openRFCOMMChannelAsync:&channel withChannelID:rfcommChannelID delegate:self] != kIOReturnSuccess ) && ( mRFCOMMChannel != nil ) )
    {
        // Something went bad (looking at the error codes I can also say what, but for the moment let's not dwell on
        // those details). If the device connection is left open close it and return an error:
        NSLog( @"Error - open sequence failed.***\n" );
        
        [self closeDeviceConnectionOnDevice:device];
        
        return FALSE;
    }
    
    mRFCOMMChannel = channel;
    
    // So far a lot of stuff went well, so we can assume that the device is a good one and that rfcomm channel open process is going
    // well. So we keep track of the device and we (MUST) retain the RFCOMM channel:
    mBluetoothDevice = device;
    [self.dataDelegate updateStatus:1];
    
    return TRUE;
}

- (void)closeDeviceConnectionOnDevice:(IOBluetoothDevice*)device
{
    if ( mBluetoothDevice == device )
    {
        IOReturn error = [mBluetoothDevice closeConnection];
        if ( error != kIOReturnSuccess )
        {
            // I failed to close the connection, maybe the device is busy, no problem, as soon as the device is no more busy it will close the connetion itself.
            NSLog(@"Error - failed to close the device connection with error %08lx.\n", (unsigned long)error);
        }
        
        mBluetoothDevice = nil;
    }
    
}

- (void)closeRFCOMMConnectionOnChannel:(IOBluetoothRFCOMMChannel*)channel
{
    if ( mRFCOMMChannel == channel )
    {
        [mRFCOMMChannel closeChannel];
    }
}

-(BOOL)writeCommandToDevice:(NSString *)command {
    // Turn the string into data.
    NSData *data = [command dataUsingEncoding:NSASCIIStringEncoding];
    NSLog(@"write: %s %lu", (void *)[data bytes], (unsigned long)[data length]);
    // Synchronously write the data to the channel.
    //return [mRFCOMMChannel writeAsync:(void *)[data bytes] length:[data length] refcon:'test'];
    return [mRFCOMMChannel writeSync:(void *)[data bytes] length:[data length]];
}

#if 0
#pragma mark -
#pragma mark These are methods that are called when "things" happen on the
#pragma mark bluetooth connection, read along and it will all be clearer:
#endif

// Called by the RFCOMM channel on us once the baseband and rfcomm connection is completed:
- (void)rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error
{
    // If it failed to open the channel call our close routine and from there the code will
    // perform all the necessary cleanup:
    if ( error != kIOReturnSuccess )
    {
        NSLog(@"Error - failed to open the RFCOMM channel with error %08lx.\n", (unsigned long)error);
        [self rfcommChannelClosed:rfcommChannel];
        return;
    }
    
    [self.dataDelegate updateStatus:2];
}

// Called by the RFCOMM channel on us when new data is received from the channel:
- (void)rfcommChannelData:(IOBluetoothRFCOMMChannel *)rfcommChannel data:(void *)dataPointer length:(size_t)dataLength
{
    unsigned char *dataAsBytes = (unsigned char *)dataPointer;
    
    while ( dataLength-- )
    {
        [self addByte:*dataAsBytes];
        dataAsBytes++;
    }
}

// Called by the RFCOMM channel on us when something happens and the connection is lost:
- (void)rfcommChannelClosed:(IOBluetoothRFCOMMChannel *)rfcommChannel
{
    // wait a second and close the device connection as well:
    [self performSelector:@selector(closeDeviceConnectionOnDevice:) withObject:mBluetoothDevice afterDelay:1.0];
    [self.dataDelegate updateStatus:0];
}

-(void)rfcommChannelWriteComplete:(IOBluetoothRFCOMMChannel *)rfcommChannel refcon:(void *)refcon status:(IOReturn)status {
    NSLog(@"refcom received %@", [self stringFromError:status]);
}

- (void)addByte:(int)byte {
    // Check for data header
    if(formerByte == 126) {
        //NSLog(@"New packet: %d\n", byte);
        if (byte == 0) {
            //NSLog(@"Accel header found\n");
            packetFlag = TRUE;
            packetByteCounter = 0;
            packetType = 0; // accel
        }
        else if (byte == 1) {
            //NSLog(@"Gyro header found\n");
            packetFlag = TRUE;
            packetByteCounter = 0;
            packetType = 1; // gyro
        }
        else if (byte == 4) {
            //NSLog(@"Command header found\n");
            packetFlag = TRUE;
            packetByteCounter = 0;
            packetType = 4; // command
        }
        else if (byte == 7) {
            //NSLog(@"Angle header found\n");
            packetFlag = TRUE;
            packetByteCounter = 0;
            packetType = 7; // angle
        }
    }
    
    if (packetFlag) {
        if (packetByteCounter > 0) {
            if (packetType == 0) {
                [self handleAccelByte:byte];
            }
            else if (packetType == 1) {
                [self handleGyroByte:byte];
            }
            else if (packetType == 7) {
                [self handleAngleByte:byte];
            }
        }
        packetByteCounter += 1;
    }
    formerByte = byte;
}

- (void)handleAccelByte:(int)byte {
    switch (packetByteCounter) {
        case 1:
            x = byte << 8;
            break;
        case 2:
            x += byte;
            break;
        case 3:
            y = byte << 8;
            break;
        case 4:
            y += byte;
            break;
        case 5:
            z = byte << 8;
            break;
        case 6:
            z += byte;
            break;
        default:
            if (x>32767){
                x=x-65536;
            }
            if (y>32767){
                y=y-65536;
            }
            if (z>32767){
                z=z-65536;
            }
            
            [_dataDelegate updateAccelWithX:x withY:y withZ:z withT:[[NSDate date] timeIntervalSince1970]];
            break;
    }
    
}

- (void)handleGyroByte:(int)byte {
    switch (packetByteCounter) {
        case 1:
            x = byte << 8;
            break;
        case 2:
            x += byte;
            break;
        case 3:
            y = byte << 8;
            break;
        case 4:
            y += byte;
            break;
        case 5:
            z = byte << 8;
            break;
        case 6:
            z += byte;
            break;
        default:
            if (x>32767){
                x=x-65536;
            }
            if (y>32767){
                y=y-65536;
            }
            if (z>32767){
                z=z-65536;
            }
            [_dataDelegate updateGyroWithRoll:x withPitch:y withYaw:z withT:[[NSDate date] timeIntervalSince1970]];
            break;
    }
    
}
- (void)handleAngleByte:(int)byte {
    //NSLog(@"cnt: %d", packetByteCounter);
    switch (packetByteCounter) {
        case 0: // ignore packet type byte
            break;
        case 1:
            x = byte;
            if(x > 127) {
                x = x - 256;
            }
            break;
        case 2:
            y = byte;
            if(y > 127) {
                y = y - 256;
            }
            break;
        case 3:
            z = byte;
            if(z > 127) {
                z = z -256;
            }
            break;
        default:
            packetFlag = false;
            if (abs(x)>3000 || abs(y)>3000 || abs(z)>3000){
                x=xFormer;
                y=yFormer;
                z=zFormer;
            }
            else{
                xFormer=x;
                yFormer=y;
                zFormer=z;
            }
            
            dy = y * 0.02;
            dz = z * 0.02;
            
            // Smoothing
            dy=0.8*dyyFormer+0.2*dy;
            dz=0.8*dzzFormer+0.2*dz;
            dyyFormer=dy;
            dzzFormer=dz;
            
            displayX=150+dy;
            displayY=150+dz;
            
            [self.dataDelegate updatePosture:displayX withY:displayY withT:[[NSDate date] timeIntervalSince1970]];
            
            break;
    }

}

//http://stackoverflow.com/questions/3887309/mapping-iokit-ioreturn-error-code-to-string
-(NSString*)stringFromError:(unsigned int)errorVal
{
    NSDictionary *ioReturnMap =
    @{@kIOReturnSuccess:          @"success",
       @kIOReturnError:            @"general error",
       @kIOReturnNoMemory:         @"memory allocation error",
       @kIOReturnNoResources:      @"resource shortage",
       @kIOReturnIPCError:         @"Mach IPC failure",
       @kIOReturnNoDevice:         @"no such device",
       @kIOReturnNotPrivileged:    @"privilege violation",
       @kIOReturnBadArgument:      @"invalid argument",
       @kIOReturnLockedRead:       @"device is read locked",
       @kIOReturnLockedWrite:      @"device is write locked",
       @kIOReturnExclusiveAccess:  @"device is exclusive access",
       @kIOReturnBadMessageID:     @"bad IPC message ID",
       @kIOReturnUnsupported:      @"unsupported function",
       @kIOReturnVMError:          @"virtual memory error",
       @kIOReturnInternalError:    @"internal driver error",
       @kIOReturnIOError:          @"I/O error",
       @kIOReturnCannotLock:       @"cannot acquire lock",
       @kIOReturnNotOpen:          @"device is not open",
       @kIOReturnNotReadable:      @"device is not readable",
       @kIOReturnNotWritable:      @"device is not writeable",
       @kIOReturnNotAligned:       @"alignment error",
       @kIOReturnBadMedia:         @"media error",
       @kIOReturnStillOpen:        @"device is still open",
       @kIOReturnRLDError:         @"rld failure",
       @kIOReturnDMAError:         @"DMA failure",
       @kIOReturnBusy:             @"device is busy",
       @kIOReturnTimeout:          @"I/O timeout",
       @kIOReturnOffline:          @"device is offline",
       @kIOReturnNotReady:         @"device is not ready",
       @kIOReturnNotAttached:      @"device/channel is not attached",
       @kIOReturnNoChannels:       @"no DMA channels available",
       @kIOReturnNoSpace:          @"no space for data",
       @kIOReturnPortExists:       @"device port already exists",
       @kIOReturnCannotWire:       @"cannot wire physical memory",
       @kIOReturnNoInterrupt:      @"no interrupt attached",
       @kIOReturnNoFrames:         @"no DMA frames enqueued",
       @kIOReturnMessageTooLarge:  @"message is too large",
       @kIOReturnNotPermitted:     @"operation is not permitted",
       @kIOReturnNoPower:          @"device is without power",
       @kIOReturnNoMedia:          @"media is not present",
       @kIOReturnUnformattedMedia: @"media is not formatted",
       @kIOReturnUnsupportedMode:  @"unsupported mode",
       @kIOReturnUnderrun:         @"data underrun",
       @kIOReturnOverrun:          @"data overrun",
       @kIOReturnDeviceError:      @"device error",
       @kIOReturnNoCompletion:     @"no completion routine",
       @kIOReturnAborted:          @"operation was aborted",
       @kIOReturnNoBandwidth:      @"bus bandwidth would be exceeded",
       @kIOReturnNotResponding:    @"device is not responding",
       @kIOReturnInvalid:          @"unanticipated driver error",
       @0:                         @"0"};
    
    return [ioReturnMap objectForKey:[NSNumber numberWithInt:err_get_code(errorVal)]];
}

@end
